from typing import Union, List, TypedDict, Literal


class WOTAccountsParams(TypedDict):
    """
    класс описывающий структуру params для запросов API accounts WOT
    """
    application_id: str
    search: str
    fields: Union[List, str]
    language: str
    limit: int
    type: str


class WOTOpenIDLoginParams(TypedDict):
    """
    класс описывающий структуру params для запросов API OPENIDLogin WOT
    """
    application_id: str
    display: Union[None, str]
    expires_at: int
    nofollow: int
    redirect_uri: str
