import datetime
import time
import pytest
import allure


# application_id field test

application_ids = [
    'beaf6570f59ff011bc9e30daa35790a3',
    'somestr'
]


@allure.suite('regress')
@allure.suite('authentication-openid')
@allure.epic('API')
@pytest.mark.parametrize('application_id', application_ids)
def testapplication_ids(wot_auth_openid_client, application_id):
    with wot_auth_openid_client(
            kwargs={
                'application_id': application_id,
            }
    ) as client:
        ur = client.get_with_auth()
        test_user_name = 'floyd'  # todo возможно это параметризовать
        if application_id == application_ids[0]:
            # проверка Выовода страницы подтверждение запроса в случае корректного application_id
            assert test_user_name in ur
        else:
            assert test_user_name not in ur


displays = ["page",
            "popup",
            'unknown'
            ]


# display field test

@allure.suite('regress')
@allure.suite('authentication-openid')
@allure.epic('API')
@pytest.mark.parametrize('displays_criteria', displays)
def testdisplay(wot_auth_openid_client, displays_criteria):
    with wot_auth_openid_client(
            kwargs={
                'search': displays_criteria,
            }
    ) as client:
        resp = client.get()
        client.url = resp.headers['Location']
        resp3 = client.get()
        # print(resp3.status_code)
        # print(resp3.headers['Location'])
        # print(resp3.url)
        assert resp.content  # todo на пк непонятно как проверить


# expires_at field test
expires_at_outline = [-1, 1, 14, 15]


@allure.suite('regress')
@allure.suite('authentication-openid')
@allure.epic('API')
@pytest.mark.parametrize('expires_at', expires_at_outline)
def testexpires_at(wot_auth_openid_client, expires_at):
    date = datetime.datetime.now() + datetime.timedelta(days=expires_at)
    expires_at = int(date.timestamp())
    with wot_auth_openid_client(
            kwargs={
                'expires_at': expires_at,
            }
    ) as client:
        ur = client.get_with_auth()
        expires = client.expires  # todo возможно это параметризовать
        if expires_at in [1, 14]:
            assert expires in ur
        else:
            # В доке дефолтное не указано
            ...


# nofollow field test
nofollows = [0, 1, 2, 'fdsf']


@allure.suite('regress')
@allure.suite('authentication-openid')
@allure.epic('API')
@pytest.mark.parametrize('nofollow', nofollows)
def testnofollows_at(wot_auth_openid_client, nofollow):
    with wot_auth_openid_client(
            kwargs={
                'nofollow': nofollow,
            }
    ) as client:
        resp = client.get()
        # проверка на редиректf
        if nofollow != 1:
            assert 'ru.wargaming.net' in resp.url
        else:
            assert 'api.worldoftanks.ru' in resp.url


# redirect_uri field test
redirect_uris = ['api.worldoftanks.ru/wot//blank/', 'yandex.ru']


@allure.suite('regress')
@allure.suite('authentication-openid')
@allure.epic('API')
@pytest.mark.parametrize('redirect_uri', redirect_uris)
def testredirect_uris_at(wot_auth_openid_client, redirect_uri):
    with wot_auth_openid_client(
            kwargs={
                'redirect_uri': redirect_uri,
            }
    ) as client:
        resp = client.get_with_auth()
        client.client.find_element_by_xpath("//*[@value='Подтвердить']").click()
        time.sleep(5)
        assert redirect_uri in client.client.current_url
