import pytest
import allure
import random
import string

# search field test

search = \
    [''.join(random.choice(string.ascii_letters + string.ascii_lowercase + string.ascii_uppercase) for i in range(25))[
     :ln]
     for ln in range(25)
     ]


@allure.suite('regress')
@allure.suite('accounts-players')
@allure.epic('API')
@pytest.mark.parametrize('search_criteria', search)
def testsearch(wot_accounts_players_client, search_criteria):
    with wot_accounts_players_client(
            api_type='account',
            kwargs={
                'search': search_criteria,
            }
    ) as client:
        resp = client.get()
        p_rest = resp.json()
        if len(search_criteria) == 0:
            r = resp.json()
            assert p_rest['error']['code'] == 402
            assert p_rest['error']['message'] == 'SEARCH_NOT_SPECIFIED'
        if 0 < len(search_criteria) < 2:
            assert p_rest['error']['code'] == 407
            assert p_rest['error']['message'] == 'NOT_ENOUGH_SEARCH_LENGTH'
        if 2 < len(search_criteria) < 24:
            assert resp.status_code == 200


# fields field test

field = [
    {
        'positive':
            [
                'nickname',
                'account_id',
                ['nickname', 'account_id'],
                ['-nickname', 'account_id'],
                ['nickname', '-account_id'],
            ]
    },

    {
        'negative':
            [
                'some_str',
                {'': ''},
                'asafd.nickname'
            ]
    }

]


@allure.suite('regress')
@allure.suite('accounts-players')
@allure.epic('API')
@pytest.mark.parametrize('field_criteria', field)
def testfields(wot_accounts_players_client, field_criteria
               ):
    data_checks = [field_criteria[i] for i in field_criteria.keys()][0]
    for data in data_checks:
        with wot_accounts_players_client(
                api_type='account',
                kwargs={
                    'search': 'floyd',
                    'fields': data,
                }
        ) as client:
            resp = client.get()
            p_rest = resp.json()
            if [k for k in field_criteria][0] == 'positive':
                assert resp.status_code == 200
            if [k for k in field_criteria][0] == 'negative':
                print(p_rest)
                assert p_rest['error']['message'] == 'INVALID_FIELDS'


langs = [
    {'positive':
         ["en", "ru", "pl", "de", "fr", "es", "zh-cn", "zh-tw", "tr", "cs", "th", "vi", "ko"]},
    {
        'negative':
            ['some_lang', ]
    }
]


# lang field test
@allure.suite('regress')
@allure.suite('accounts-players')
@allure.epic('API')
@pytest.mark.parametrize('lang_criteria', langs)
def testlangs(wot_accounts_players_client, lang_criteria):
    check_type = list(lang_criteria.keys())[0]
    for lng in lang_criteria[check_type]:
        with wot_accounts_players_client(
                api_type='account',
                kwargs={
                    'search': 'floyd',
                    'language': lang_criteria[check_type],
                }
        ) as client:
            resp = client.get()
            p_rest = resp.json()
            if check_type == 'positive':
                assert p_rest['status'] == 'ok'
            else:
                assert p_rest['status'] == 'error'
                assert p_rest['error']['message'] == 'INVALID_LANGUAGE'
                assert p_rest['error']['code'] == 407


# limit field test

limints = [random.randint(0, 100), 102]


@allure.suite('regress')
@allure.suite('accounts-players')
@allure.epic('API')
@pytest.mark.parametrize('limits_criteria', limints)
def testlimit(wot_accounts_players_client, limits_criteria):
    with wot_accounts_players_client(
            api_type='account',
            kwargs={
                'search': 'alex',
                'limit': limits_criteria
            }
    ) as client:
        resp = client.get()
        p_rest = resp.json()
        assert p_rest['meta']['count'] == limits_criteria if limits_criteria < 100 else 100


# type field test

field_types = ["startswith", "exact", 'unknown']


# TODO - на полноценные проверки startswith и exact хоть отдельный модуль пиши, но я реализую по простому для примера
@allure.suite('regress')
@allure.suite('accounts-players')
@allure.epic('API')
@pytest.mark.parametrize('field_type', field_types)
def testtype(wot_accounts_players_client, field_type):
    with wot_accounts_players_client(
            api_type='account',
            kwargs={
                'search': 'floyd',
                'type': field_type
            }
    ) as client:
        resp = client.get()
        p_rest = resp.json()
        assert p_rest['status'] == 'ok' if field_type in ["startswith", "exact"] else 'error'
