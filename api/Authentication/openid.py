import time
import requests
from api import BaseApi
import datetime
import httpx
import os
import pathlib
from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support.wait import WebDriverWait
from seleniumrequests import Chrome
from selenium.webdriver import ChromeOptions


class WOTAuthOpenIDLoginApi(BaseApi):
    """
    Класс-обертка отправки запросов с параметрами в Authentication - OpenIDLogin
    """

    def __init__(self, kwargs: dict):
        from models.params_models import WOTOpenIDLoginParams
        super().__init__()
        self.url = f'https://api.worldoftanks.ru/wot/auth/login/'
        # Парсинг параметров возможно передаваемых
        base_date = datetime.datetime.now() + datetime.timedelta(days=1)
        self.expires = int(base_date.timestamp())
        self.client = httpx.Client()
        self.req_client = None
        self.params: WOTOpenIDLoginParams = {
            'application_id': 'beaf6570f59ff011bc9e30daa35790a3',
            'display': kwargs.get('display', None),
            'expires_at': kwargs.get('expires_at', self.expires),
            'nofollow': kwargs.get('nofollow', 0),
            'redirect_uri': kwargs.get('redirect_uri',
                                       'https://developers.wargaming.net/reference/all/wot/auth/login/'),
        }
        self.follow_redirects = True

    def post(self, *args, **kwargs):
        return self.req_client.post(
            url=self.url,
            params=self.params,
            headers=self.headers,
            cookies=self.cookies,
            auth=self.auth,
            timeout=self.timeout,
        )

    def get(self):
        return self.client.get(
            url=self.url,
            params=self.params,
            headers=self.headers,
            cookies=self.cookies,
            auth=self.auth,
            follow_redirects=self.follow_redirects,
            timeout=self.timeout,
            extensions=self.extensions
        )

    def get_with_auth(self):
        # метод для полноценной авторизации пользователем для проверки E2E OPENID LOGIN ответов

        options = ChromeOptions()
        # options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        # переопределение клиента для запросов на хром для автополучения куков и простой авторизации
        self.client = Chrome(executable_path=os.path.join(pathlib.Path(__file__).parent.parent.parent.absolute(),
                                                          'tools', 'chromedriver.exe'), options=options)
        self.req_client = requests.Session()
        self.pre_request_auth_actions()
        self.url = self.req_client.get(
            url=self.url,
            params=self.params,
        ).url
        self.client.get(self.url)
        time.sleep(5)
        return self.client.current_url

    def put(self):
        ...

    def pre_request_auth_actions(self):
        self.client.get('https://ru.wargaming.net/id/signin/?')
        login = "//input[@placeholder='Электронная почта']"
        pas = "//input[@placeholder='Пароль']"
        enter_button = "//*[.='Войти']"
        WebDriverWait(self.client, 5).until(presence_of_element_located((By.XPATH, login)))
        Warning('параметризовать креды пользователя перд запуском тестов environ TEST_WOT_LOGIN, TEST_WOT_PASSWORD')
        self.client.find_element_by_xpath(login).send_keys(os.environ.get('TEST_WOT_LOGIN'), 'TEST_WOT_LOGIN') # todo это параметризовать перд запуском
        self.client.find_element_by_xpath(pas).send_keys(os.environ.get('TEST_WOT_PASSWORD'), 'TEST_WOT_PASSWORD') # todo это параметризовать перд запуском
        self.client.find_element_by_xpath(enter_button).click()
        # простой слип, не заморачиваюсь для теста с ожиданиями
        time.sleep(3)
        [self.req_client.cookies.set(c['name'], c['value']) for c in self.client.get_cookies()]

    def __aexit__(self, exc_type, exc_val, exc_tb):
        self.client.close()
        if self.req_client:
            self.req_client.close()
