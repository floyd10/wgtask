import typing
from abc import ABC
import httpx
from httpx._client import UseClientDefault, USE_CLIENT_DEFAULT
from httpx._types import QueryParamTypes, HeaderTypes, CookieTypes, AuthTypes, TimeoutTypes


class BaseApi(ABC):
    """
    Базовый класс-обертка отправки сформированных запросов api
    """
    def __init__(self):
        self.url = None
        self.params: QueryParamTypes = None
        self.headers: HeaderTypes = None
        self.cookies: CookieTypes = None
        self.auth: typing.Union[AuthTypes, UseClientDefault] = USE_CLIENT_DEFAULT
        self.follow_redirects: typing.Union[bool, UseClientDefault] = USE_CLIENT_DEFAULT
        self.timeout: typing.Union[TimeoutTypes, UseClientDefault] = USE_CLIENT_DEFAULT
        self.extensions: dict = None
        self.client = httpx.Client()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.client.close()

    def post(self):
        ...

    def get(self):
        ...

    def put(self):
        ...