from api import BaseApi


class WOTAccountsPlayersApi(BaseApi):

    """
    Класс-обертка отправки запросов с параметрами в Accounts - Players
    """

    def __init__(self, api_type: str, kwargs: dict):
        from models.params_models import WOTAccountsParams
        super().__init__()
        self.url = f'https://api.worldoftanks.ru/wot/{api_type}/list/?'
        # Парсинг параметров возможно передаваемых
        self.params: WOTAccountsParams = {
            'application_id': '665706907130c05ec28aa85c64eb6589',
            'search': kwargs.get('search', ''),
            'fields': kwargs.get('fields', ''),
            'language': kwargs.get('language', 'ru'),
            'limit': kwargs.get('limit', 10),
            'type': kwargs.get('type', 'startswith')
        }

    def post(self):
        ...

    def get(self):
        return self.client.get(
            url=self.url,
            params=self.params,
            headers=self.headers,
            cookies=self.cookies,
            auth=self.auth,
            follow_redirects=self.follow_redirects,
            timeout=self.timeout,
            extensions=self.extensions
        )

    def put(self):
        ...
