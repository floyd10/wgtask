import pytest
from api.Accounts.players import WOTAccountsPlayersApi
from api.Authentication.openid import WOTAuthOpenIDLoginApi


@pytest.fixture
def wot_accounts_players_client():
    yield WOTAccountsPlayersApi


@pytest.fixture
def wot_auth_openid_client():
    yield WOTAuthOpenIDLoginApi
